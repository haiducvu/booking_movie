import {USER_PROFILE} from '../../constants/constant'
import {ProfileService} from '../../services/profile';
import {storeCredentials} from '../../utils/LocalStorage/LocalStorage'

const profile=(userData) =>{
  const {data}= userData;
  return {
    type: USER_PROFILE,
    payload: data
  }
}

export const inforUserAction=(taiKhoan)=> {
  return(dispatch)=>{
    ProfileService(taiKhoan)
      .then(res=>{
        dispatch(profile(res));
        const {data}= res;
        storeCredentials(JSON.stringify(data));
      })
      .catch(err=>{
        console.log(err);
      });
  }
}