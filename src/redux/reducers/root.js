import { combineReducers } from 'redux';
import UserReducer from './userReducer';
import ProfileReducer from './profileReducer'

const RootReducer = combineReducers({
  user: UserReducer,
  profile: ProfileReducer,
});
export default RootReducer;
