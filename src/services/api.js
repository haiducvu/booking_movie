const LOGIN_API = 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap';
const REGISTER_API = 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/DangKy';
const USER_PROFILE_API = 'https://movie0706.cybersoft.edu.vn/api/QuanLyNguoiDung/ThongTinTaiKhoan';

export {LOGIN_API, REGISTER_API, USER_PROFILE_API};